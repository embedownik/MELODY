# MELODY module - for playng simple melodies with buzzer

Example initialization - configuration for "BUZZER" module:

```C
static melody_config_s melody_config =
{
     .playFreq_cb = buzzer_Play,
     .stop_cb     = buzzer_Stop,
};

melody_Init(melody_config);
```

Example song:

```C
static melody_singleNote_s notes[] =
{
 {
  .duration  = 10U,
  .frequency = 300U,
 },
 {
  .duration  = 10U,
  .frequency = 400U,
 },
 {
  .duration  = 10U,
  .frequency = 300U,
 },
 {
  .duration  = 10U,
  .frequency = 400U,
 },
 {
  .duration  = 10U,
  .frequency = 300U,
 },
 {
  .duration  = 10U,
  .frequency = 400U,
 },
};

static melody_melody_s melody1 =
{
 .length = 5U,
 .notes = notes,
};
```

Requirements/usage:

Insert melody_Tick() function inside cyclic timer callback.

No platform dependencies.
