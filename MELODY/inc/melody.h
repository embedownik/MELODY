/*
 * melody.h
 *
 *      Author: embedownik
 */

#ifndef MELODY_MELODY_INC_MELODY_H_
#define MELODY_MELODY_INC_MELODY_H_

#include <stdint.h>

/**
 * Callback to use - to play selected frequency.
 * @param freq frequency to play
 */
typedef void (*melody_playFreq_cb)(uint16_t freq);

/**
 * Callback to use - to stop melody..
 */
typedef void (*melody_stop_cb)(void);

/**
 * Config for module init.
 */
typedef struct
{
    melody_stop_cb     stop_cb;
    melody_playFreq_cb playFreq_cb;
}melody_config_s;

/**
 * Info about single note to play.
 */
typedef struct
{
    uint16_t frequency;
    uint8_t  duration;
}melody_singleNote_s;

/**
 * Info about melody to play.
 */
typedef struct
{
    const uint8_t             length;
    const melody_singleNote_s *notes;
}melody_melody_s;

void melody_Init(melody_config_s config);

/**
 * Play selected melody
 * @param melody melody to play
 */
void melody_PlayMelody(const melody_melody_s *melody);

/**
 * Stop actual melody.
 */
void melody_Stop(void);

/**
 * Function to call in timer handler.
 * For example if this function is called every 1ms - timebase for melodies is 1ms.
 */
void melody_Tick(void);

#endif
