#include <stddef.h>

#include <melody.h>

typedef enum
{
    melody_noMelody,
    melody_toStop,
    melody_playing,
    melody_newMelody,
}melody_state_e;

typedef struct
{
    uint8_t         melodyPositionCounter; /* actual position in melody */
    uint8_t         melodyExpectedDuration;/* "buffer" for melody duration instead of reading from melody array */
    uint8_t         noteDurationCounter;   /* actual note duration counter in ticks */
    uint8_t         noteExpectedDuration;  /* "buffer" for duration instead of reading from melody array */

    melody_state_e  state;                 /* module state from enum */

    const melody_melody_s *actualMelody;   /* actual played melody */
    const melody_melody_s *nextMelody;     /* melody to play - in next Tick event */
}melody_status_s;

/* config from init function */
static melody_config_s actualConfig;

static melody_status_s actualStatus;

static void playNote(uint8_t noteNumber)
{
    uint16_t freqToPlay = actualStatus.actualMelody->notes[noteNumber].frequency;

    actualConfig.playFreq_cb(freqToPlay);

    actualStatus.noteExpectedDuration = actualStatus.actualMelody->notes[noteNumber].duration;
}

void melody_Init(melody_config_s config)
{
    actualConfig = config;
}

void melody_PlayMelody(const melody_melody_s *melody)
{
    actualStatus.nextMelody = melody;

    actualStatus.state = melody_newMelody;
}

void melody_Stop(void)
{
    actualStatus.state = melody_toStop;
}

void melody_Tick(void)
{
    switch(actualStatus.state)
    {
        case melody_noMelody:
        {
            /* Do nothing */
            break;
        }
        case melody_toStop:
        {
            actualStatus.state = melody_noMelody;
            actualConfig.stop_cb();

            break;
        }
        case melody_playing:
        {
            actualStatus.noteDurationCounter++;

            if(actualStatus.noteDurationCounter == actualStatus.noteExpectedDuration)
            {
                actualStatus.noteDurationCounter = 0U;

                /* play the next note */
                actualStatus.melodyPositionCounter++;

                /* last note in melody */
                if(actualStatus.melodyPositionCounter == actualStatus.melodyExpectedDuration)
                {
                    actualStatus.state = melody_noMelody;
                    actualConfig.stop_cb();
                }
                else
                {
                    /* play next note */
                    playNote(actualStatus.melodyPositionCounter);
                }
            }

            break;
        }
        case melody_newMelody:
        {
            actualStatus.actualMelody = actualStatus.nextMelody;
            actualStatus.state = melody_playing;

            playNote(0U);

            actualStatus.melodyPositionCounter = 0U;
            actualStatus.melodyExpectedDuration = actualStatus.actualMelody->length;
            actualStatus.noteDurationCounter = 0U;

            if(actualStatus.actualMelody == NULL)
            {
                actualStatus.state = melody_noMelody;
            }

            break;
        }
        default:
        {
            /* Do nothing */
            break;
        }
    }
}
